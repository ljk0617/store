import	wordcloud
import	jieba
txt	= "程序设计语言是计算机能够理解和\  识别用户操作意图的一种交互体系，它按照特定规则组织计算机指令，使计算机能够自\ 动进行各种运算处理。"
w = wordcloud.WordCloud( \
    font_path="simsun.ttc", width=1000, height=700, \
  #  background_color = "white"
    )
w.generate(" ".join(jieba.lcut(txt)))
w.to_file("andy2.png")