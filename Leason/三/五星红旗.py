import turtle
import math
turtle.pencolor("red")
turtle.fillcolor("red")
turtle.pensize(1)

#画矩形，选择的66*44比例的旗帜
turtle.begin_fill()
turtle.up()
turtle.goto(-330,-220)
turtle.down()
for i in range(2):
    turtle.fd(660)
    turtle.left(90)
    turtle.fd(440)
    turtle.left(90)
#此时海龟坐标为-330,-220
turtle.end_fill()
#分割二象限，用于调试计算，提交时注释掉
'''turtle.up()
turtle.goto(-330,0)
turtle.down()
turtle.fd(660)

turtle.speed(0)
turtle.up()
turtle.goto(0,220)
turtle.down()
turtle.seth(-90)
turtle.fd(440)

for i in range(10):   
    turtle.up()
    turtle.goto(-330,22*i)
    turtle.seth(0)
    turtle.down()
    turtle.fd(330)


for i in range(15):   
    turtle.up()
    turtle.goto(-22*i,220)
    turtle.seth(-90)
    turtle.down()
    turtle.fd(220)

turtle.up()
turtle.goto(-220,44)# 大五角星的中心的正下方
turtle.down()
turtle.seth(0)
turtle.circle(66)
'''
#画五角星
def draw_star(x,y,a,b,rank,radis):
    turtle.fillcolor("yellow")#填充颜色
    turtle.pencolor("yellow") #画笔颜色
    turtle.up()
    pos_list=[] #存储五个点的坐标的二维列表
    turtle.goto(x,y)
    turtle.seth(0)
    if rank>0:
        if(rank<3):#判断是上面两个星星还是下面两个星星，因为转的角度不同
            rank=2
        else:
            rank=3
        f=90*rank+math.degrees(math.atan(a/b))
    else:
        f=0;
    #print(f)
    turtle.circle(-radis,f)
    for i in range(5): #循环5次，标记五个点
        turtle.up()
        turtle.circle(-radis,144)
        #turtle.down()
        #turtle.dot()
        pos_list.append(turtle.pos())
    #print(small_pos_list)
    turtle.up()
    turtle.goto(pos_list[4])#回到起点
    turtle.begin_fill()
    turtle.down()
    for i in range(5): #开始画五角星
        turtle.goto(pos_list[i])
    turtle.end_fill()


draw_star(-220,176,0,0,0,66) #画大五角星，输入起点坐标，偏置角度0，0，规定大五角星编号为0，圆形半径为66
draw_star(-110,198,5,3,1,22) # 第一颗星星 起点在左边第5个格子，上边9个格子，偏置角度的tan=5/3,编号为1，圆形半径为22
draw_star(-66,154,7,1,2,22)# 第二颗星星 起点在左边第3个格子，上边7个格子，偏置角度的tan=7/1,编号为2，圆形半径为22
draw_star(-66,88,2,7,3,22)# 第三颗星星 起点在左边第3个格子，上边7个格子，偏置角度的tan=2/7,编号为3，圆形半径为22
draw_star(-110,44,4,5,4,22) # 第四颗星星 起点在左边第5个格子，上边9个格子，偏置角度的tan=4/5,编号为4，圆形半径为22

turtle.done()
